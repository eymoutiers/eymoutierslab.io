# Publier sur le site

Voici quelques informations utiles pour les rédacteurs et rédactrices du site.

Note: _en cours de rédaction_.

**Pour publier un nouvel articles**

- Aller dans la `Section` concernée (colonne latérale): exemple la section `Blog`.
- Cliquer sur `Create New` et choisir le format.
- Vous êtes sur l’interface de l’éditeur de texte.
- Par défaut tout nouveau contenu est en mode `Draft` (brouillon). Penser à mettre cette option en _off_ pour publier (bouton en haut à droite).

**Pour modifier un article déjà publié**

- Aller dans la `Section` concernée (colonne latérale).
- Cliquer sur le titre de l’article que l'on souhaite modifier.
- Vous êtes sur l’interface de l’éditeur de texte.
- Une fois les modifications faites: cliquer sur `Save`. C’est fait.
