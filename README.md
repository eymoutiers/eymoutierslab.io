# Les écrits d’août

**Site pour “Les écrits d’août” Eymoutiers**    
Rencontres littéraires 16-19 août 2019    
[https://ecrits-aout.fr/](https://ecrits-aout.fr/)

***

Le site est publié avec [Hugo](https://gohugo.io/)

### Versions des librairies

- Bootstrap : 4.3.1
- JQuery : 3.31-Slim
- Popper.js : 1.14.7
- espaceFine.js

### Fontes utilisées

- Fork Awesome : 1.1.7 + Compatibilité Font Awesome v5
- Merriweather (hébergée sur Google Font)
- Montserrat (hébergée sur Google Font)

***

### Todo

- [x] Passer à Bootstrap 4.3.1
- [x] Mettre à jour les librairies JS (JQuery, Popper, etc.)
- [x] Déplacer l'admin de Forestry.io dans `static/` (erreur de conf)
- [ ] Tester le site avec une version plus récente de Hugo
- [ ] Passer à SASS pour pouvoir faire “maigrir” un peu Bootstrap
- [x] Désactiver `highlight.js` (non utilisé sur ce site)
- [ ] Personnaliser les flux RSS et ajouter des flux Atom (et json ?)
- [ ] Installer un moteur de recherche (Lunar.js ?)
- [ ] Héberger localement les polices de caractères (Merriweather, Montserrat)
- [ ] Tester le _shortcode_ pour inclure des vidéos depuis Peertube
- [x] Alléger la CSS Fork Awesome (fin de compatibilité Font Awesome)
- [ ] Mettre en place le trairement des images par Hugo (si possible)
- [ ] Revoir les CSS pour les vidéos et `iframes` embarquées

***

### Licences

- [The MIT License](https://opensource.org/licenses/mit-license.html)
- [GNU general public license version 2](https://opensource.org/licenses/GPL-2.0)
- [SIL Open Font License ((OFL-1.1)](https://opensource.org/licenses/OFL-1.1)
- [Do What The Fuck You Want To Public License (WTFPL)](http://www.wtfpl.net/about/)

***

### liens

- https://gohugo.io/
- https://git-scm.com/
- https://getbootstrap.com/
- https://jquery.org/
- https://highlightjs.org/
- https://popper.js.org/
- https://forkaweso.me/
