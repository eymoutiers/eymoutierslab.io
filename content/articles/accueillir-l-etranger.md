---
date: 2019-08-16T10:41:36+00:00
title: Accueillir l’étranger
featured_image: "/photo-cp20190816-parrot.jpg"
description: Un banquet préparé avec l’association Montagne Accueil Solidarité et
  une rencontre avec Karine Parrot, Victor Colle et Nicolas Fargues
categories:
- Programme

---
A partir de 13 heures, est organisé sous  les arcades du Foyer de la mairie, le banquet préparé avec l’association **Montagne Accueil Solidarité** et, dans le prolongement (jusqu’à 15H), une rencontre avec Karine Parrot autour de son livre _Carte blanche. L’État contre les étrangers_, Victor Collet, auteur de _Nanterre, du Bidonville à la cité_ et Nicolas Fargues.

**Victor Collet** a vécu, mené ses recherches et milité dix ans à Nanterre, des luttes de quartier à la défense des étrangers et auprès des habitants des bidonvilles d’aujourd’hui.

**Nicolas Fargues** a publié douze romans aux éditions P.O.L. Enfance au Cameroun, au Liban puis en Corse, études à Paris. Deux ans de coopération en Indonésie, retour à Paris, petits boulots, publication en 2000 du _Tour du propriétaire_. Depuis 2002, a vécu à Diego-Suarez, Yaoundé et Wellington.

**Karine Parrot** est professeure de droit à l’Université de Cergy-Pontoise, membre du GISTI (Groupe d’information et de soutien des immigré.es).

**Montagne Accueil Solidarité** (MAS) est une association locale sur Eymoutiers investie dans le travail de terrain en soutien aux sans-papiers et réfugiés. Elle a organisé en décembre 2018 la rencontre Les [« Poétiques du refuge »](http://www.culture.gouv.fr/Regions/Drac-Nouvelle-Aquitaine/Actualites/Les-Poetiques-du-Refuge-Dejouer-les-frontieres-a-Eymoutiers-les-18-et-19-decembre-2018) et précédemment les [Rencontres du droit d’asile](https://www.lacimade.org/eymoutiers-87/) (2016).

***
Liens

* [Le programme](https://ecrits-aout.fr/documents/le-programme/ "Le programme 2019")
* [Les auteurs](https://ecrits-aout.fr/documents/les-auteurs/ "Les auteurs")

***
Montagne Accueil Solidarité (MAS)    
16, boulevard d’Aigues-Vives -- 87120 Eymoutiers     
