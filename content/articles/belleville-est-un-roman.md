---
date: 2019-08-14T02:00:00.000+00:00
title: Belleville est un roman
featured_image: "/photo-14468592639-a539bf2a63.jpg"
description: Déambulation filmée dans les rues du quartier parisien de Belleville
  avec Serge Quadruppani,  et quelques autres figures du quartier. Projection au Jean-Gabin
categories:
- Programme
---
Dans le cadre des **Écrits d’août**, au Jean-Gabin, le cinéma municipal d'Eymoutiers, projection samedi 17 d'une rareté : _Belleville est un Roman_ (1996),un film de **José Reynès** sur une idée de moi-même personnellement, où on me voit déambuler dans Belleville et parler avec quelques-unes des figures du quartier.

> Des décorations kitsch, accrochées aux volets du rez-de-chaussée d'une vieille maison, avaient retenu mon attention et, à partir de là, j’ai construit un personnage de mon roman noir, _Rue de la Cloche_. J'ai imaginé que vivait là une dame, sympathique, pocharde qui conservait des poupées baptisées chacune du nom d'un de ses enfants, tous confiés à l'Assistance publique. J'ai eu la surprise de découvrir qu’en réalité, elle a la garde de nombreux enfants sans parents qu'elle élève au milieu d'animaux...

Un film où vous verrez aussi **Thierry Jonquet**, **Jean Echenoz**, **Patrick Raynal** parler avec bibi de notre quartier et des personnages qui ont pu nous inspirer… et des bouts de vie dans ce qui était (est ?) encore un des derniers quartiers populaires de **Paname**.

Le tout précédé de quelques rares courts métrages de **Tito Topin** et **Jean Yanne**. Quand on vous dit que la vie est intense dans le Limousin…

**Serge Quadruppani**

***

Liens

* [Le programme](https://ecrits-aout.fr/documents/le-programme/ "Le programme 2019")
* [José Reynès](http://www.film-documentaire.fr/4DACTION/w_liste_generique/C_1009_F "José Reynès")

***

<p class="text-muted"> Photo de Magalie M. (<a href="https://www.flickr.com/photos/mistinguette18/">Creative Commons by-nc-nd</a>) </p>
