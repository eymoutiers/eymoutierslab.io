---
title: "Damasio, Dufresne, Lordon : Résistance, résistances"
date: 2019-08-23T01:25:40.000+00:00
featured_image: "photo-b35fdd55d5cb7a47.jpeg"
description: Retour vidéo de Télé Millevaches sur le débat organisé, avec l’Université populaire d’Eymoutiers, autour du thème « Résistance, résistances »
layout: "single_nb"
categories:
- Compte-rendus
- Vidéos
---

{{< vimeo 355107260 >}}

<div class="pb-5"></div>


L’équipe de **Télé Millevaches** était présente lundi 19 août à Eymoutiers pour le débat organisé, en lien avec l’Université populaire d’Eymoutiers, sur le thème « Résistance, résistances ».

> C’est le _Dictionnaire amoureux de la Résistance_, de Gilles Perrault, qui sert de point de départ à cette rencontre. L’esprit de la Résistance peut-il encore nous être utile aujourd’hui ? Peut-on le voir à l’œuvre dans les mouvements des peuples actuels ?

En l’absence de Gilles Perrault, [excusé](https://ecrits-aout.fr/articles/gilles-perrault-partie-remise/) pour raison de santé, c’est **Alain Damasio**, **David Dufresne** et **Frédéric Lordon** (dans le rôle de l’invité surprise) qui se mettent à l’ouvrage...

> Cette question qui leur est proposée, Alain Damasio, David Dufresne et Frédéric Lordon la débordent bien vite. Peut-on échapper à la société de contrôle ou doit-on la renverser ? Est-il possible de construire un archipel de dissidences qui résiste à la répression ? Le Grand soir n’est-il qu’un fossile d’une pensée politique ensevelie ?

Cette vidéo de 77 minutes, réalisée par **Télé Millevaches**, restitue l’un des moments important ([parmi tant d’autres](https://ecrits-aout.fr/articles/toutes-nos-esperances/)) qui on fait les **Ecrits d’août**.

Liens

- [Télé Millevaches](https://telemillevaches.net/videos/damasio-dufresne-lordon-resistance-resistances/)
- [Université populaire d’Eymoutiers](http://www.up-eymoutiers.fr/)

***

Vidéo sous licence Creative Commons Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions 3.0 France (CC BY-NC-SA 3.0 FR)
