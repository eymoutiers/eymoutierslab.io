---
date: 2019-08-15T17:35:00+00:00
title: 'Eric Vuillard : « Il faut écrire ce qu''on ignore »'
featured_image: "/photo-lundimatin-1901resp1280-2.jpg"
description: Le dimanche 18 août, à l’occasion de la journée des auteurs, aura lieu
  une présentation de l’œuvre d’Eric Vuillard par Pascal Léonard
categories:
- Programme

---
> Il faut écrire ce qu'on ignore. Au fond, le 14 Juillet, on ignore ce qui se produisit. Les récits que nous en avons sont empesés ou lacunaires. C'est depuis la foule sans nom qu'il faut envisager les choses. Et l'on doit raconter ce qui n'est pas écrit. Il faut le supputer du nombre, de ce qu'on sait de la taverne et du trimard, des fonds de poche et du patois des choses, liards froissés, croûtons de pain.

[14 juillet](https://www.actes-sud.fr/catalogue/litterature/14-juillet), Éric Vuillard, Actes Sud, 2016.

> Toute écriture emporte avec elle les contradictions de l’Histoire et l’espoir de les dépasser. On trouve ainsi dans _Les Misérables_ une sorte de fraternité avec les pauvres presque séditieuse mais aussi un banal sentimentalisme bourgeois, on y trouve un ébranlement qui remet tout en cause et un attendrissement charitable. Le style, le lyrisme de Victor Hugo porte la marque de cette discordance. Il y a dans ce très grand livre un mélange indémêlable d’éloquence qui emporte et d’emphase qui embaume, de rythmes puissants et de pathos. Comme dans toute œuvre, il y a quelque chose d’inadéquat dans la prose de Victor Hugo, une sorte de conflit entre le trop près et le trop loin, entre la sensibilité et la compassion.

[Dialogue avec Eric Vuillard](https://lundi.am/Dialogue-avec-Eric-Vuillard), Lundimatin, mars 2019

Éric Vuillard est écrivain et cinéaste. Il a réalisé deux films, _L’homme qui marche_ (2006) et _Mateo Falcone_ (2008). Il a reçu le prix Franz-Hessel 2012 et le prix Valery-Larbaud 2013 pour deux récits publiés chez Actes Sud, _La bataille d’Occident_ (2012) et _Congo_, (2012) ainsi que le prix Joseph-Kessel 2015 pour _Tristesse de la terre_ (2014), le prix Alexandre Viallate pour _14 juillet_ (2017) et le prix Goncourt 2017 pour _L’ordre du jour_ (2017). Son dernier livre paru est _La guerre des pauvres_ (Actes Sud, 2018).

Le dimanche 18 août, à l’occasion de la journée des auteurs, aura lieu une présentation de l’œuvre d’Eric Vuillard par Pascal Léonard. Création littéraire et musicale de Julien Boudart et Eric Vuillard.

***

Liens

* [Le programme](https://ecrits-aout.fr/documents/le-programme/ "Le programme 2019")
* [Eric Vuillard](https://www.actes-sud.fr/contributeurs/vuillard-eric-0 "Actes Sud")

***

<p class="text-muted"> Photo publiée sur le site Lundimatin</p>