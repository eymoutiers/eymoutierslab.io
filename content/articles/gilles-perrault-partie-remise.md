---
title: "Gilles Perrault : partie remise"
date: 2019-08-13T04:57:40.000+00:00
featured_image: "/photo-9195417043912884224.jpg"
description: "Gilles Perrault va devoir subir une intervention chirurgicale en urgence, il doit donc annuler sa participation... La rencontre “Résistance, résistances” est maintenue, animée par Serge Quadruppani, Jérôme Leroy, David Dufresne, Alain Damasio et Frédéric Lordon"
categories:
- Programme
- Infos
---
**Mauvaise nouvelle** : Gilles Perrault va devoir subir une intervention en urgence pour sauver le peu de vue qui lui reste, l’opération est programmée pour lundi. Gilles a donc dû **annuler** sa participation aux Ecrits d’Août. On l’a bien senti dans sa voix, il avait la mort dans l’âme quand il nous l’a annoncé, ajoutant qu’il s’était tellement réjoui de nous revoir, ainsi que Jérôme Leroy (Cornélius Rouge) et David Dufresne, et de faire connaissance avec les autres.

Comme nous sommes obstinés, nous lui disons juste : partie remise. **Tiens bon, vieux camarade**, il faut que tu puisses continuer à lire toutes les bêtises exécrables qui nourrissent ton indignation dans ces journaux papier que tu t’obstines à acheter à l’ancienne.

Mais la rencontre **« Résistance, résistances »**, du lundi 19 à 19h30 dans la grande salle de la mairie d’Eymoutiers (4e étage) est maintenue, Jérôme Leroy et moi nous parlerons de ton livre _Dictionnaire amoureux de la Résistance_, David Dufresne, Alain Damasio et Frédéric Lordon, nous aideront à illustrer la deuxième partie du titre: les résistances aujourd’hui et leurs rapports, ou non, avec la Résistance.

**Serge Quadruppani**

----
<p class="text-muted">
Photo Serge Quadruppani
</p>
