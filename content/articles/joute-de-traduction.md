---
date: 2019-08-13T09:00:00.000+00:00
title: "Lire l’étranger : joute de traduction"
featured_image: "/image-20190813-botte-de-nevers.jpeg"
description: "Transposer d’une langue dans une autre, ce n’est pas seulement mettre
  en relation deux vocabulaires et deux syntaxes, c’est faire communiquer deux univers
  mentaux"
categories:
- Programme

---
Transposer d’une langue dans une autre, ce n’est pas seulement mettre en relation deux vocabulaires et deux syntaxes, c’est faire communiquer deux univers mentaux.

Après une conférence, le dimanche 11, dans le cadre du festival **Le Théâtre Rate**, d’André Markowicz, mythique traducteur, volcan en éruption permanente de traductions (surtout du russe) et de réflexions, vous pourrez approcher, dans le cadre des **Écrits d’Août**, le vendredi 16 (à 17h30), la réalité concrète de cette activité tout à fait impossible et absolument nécessaire : traduire.

Pour ce rendez-vous, placé sous le signe de _L’étranger_ (aller chez l’étranger, accueillir l’étranger, lire l’étranger, écouter l’étranger), nous avons imaginé que cela pouvait prendre la forme d’une **joute de traduction**, une mise en jeu bienveillante de leur traduction d’un même texte.

> Béatrice Roudet-Marçu et Hervé Denès se pencheront sur un court texte, inédit en français, d’un auteur anglophone. Chacun aura préparé sa traduction à l’avance et viendra la _confronter_ à celle de son collègue. Devant un écran présentant le texte original encadré des deux traductions, l’une et l’autre défendront, bien entendu amicalement, leurs choix de traduction et accueilleront volontiers les questions et suggestions du public.

Angliciste et traductrice littéraire au talent reconnu, **Béatrice Roudet-Marçu** connaît tous les aspects du métier grâce à sa participation à diverses instances dont la commission d’aide à la traduction du Centre national du livre (CNL). Elle traduit des romans d’auteurs africains, anglais, nord-américains (Michiel Heyns, Sue Miller, James Patterson…) Elle traduit également pour la presse et pour des institutions internationales. Après avoir enseigné l’interprétariat à l’Université de Londres, elle est aujourd’hui chargée de cours à l’Ecole de Traduction Littéraire.

Traducteur de l’anglais (_Zelda Popkin, Congés pour meurtre ; Boxcar Bertha, Une autobiographie…_), du chinois (Li Jingze, _Relations secrètes_ ; Xu Zecheng, _La Grande harmonie_…), de l’italien et de l’espagnol, **Hervé Denès** est aussi l’auteur de deux livres politiques sur la Chine; en collaboration avec Charles Reeve, et de  _Douceur de l’aube. Souvenirs doux-amers d’un Parisien dans la Chine de Mao_ (L’insomniaque, 2015), un émouvant récit de souvenirs sur un amour impossible dans la Chine à la veille de la Révolution culturelle.

***

Liens

* [Le programme](https://ecrits-aout.fr/documents/le-programme/ "Le programme 2019")
* [L’Insomniaque](http://www.insomniaqueediteur.com "Les éditions de l’Insomniaque")

***

<p class="text-muted"> La botte de Neuvers, gravure anonyme, sans date (domaine public). </p>
