---
date: 2019-08-15T13:12:06+00:00
title: "Golo, ou la magie du roman graphique"
featured_image: "/image-golo-bck91p0-2048.jpg"
description: "L’auteur de bande-dessinées Golo s’expose du 16 au 19 août à Eymoutiers.
  Petite biographie subjective"
categories:
- Apartés
- Programme

---
> Ma vie m’appartient, seul mes livres appartiennent au public.   
> -- B. Traven

Golo, né Guy Nadaud à Bayonne en 1948, commence sa carrière de dessinateur au début des années 1970 dans le mensuel musical _Best_. A partir de 1978, ses histoires dessinées et ses illustrations seront aussi publiées dans divers titres phares de la presse spécialisée de l’époque : _Hara-Kiri_, _Charlie Hebdo_, _Pilote_, _Actuel_ et surtout _Charlie Mensuel_, à qui l’on doit la découverte ou redécouverte de nombreux talents de la bande-dessinée, de Crepax à Munoz et Sampayo, en passant par Buzzelli, Brescia, Sterenko ou quelqu’un comme Golo justement.

En 1979 il signe, avec son complice Frank (Frank Reichert), le roman graphique _Balade pour un voyou_, aux éditions du Square (collection « Bouquins Charlie »). L’album est remarqué et remarquable, tant par le dessin un peu _trash_, un découpage des scènes et un cadrage de cases très cinématographique, le tout pour servir une narration haletante d’un style très « noir ».

Le succès en librairie est au rendez-vous, même si relativement modeste. _Libération_, _L’Echo des savanes_ ou le mensuel _(A suivre)_ (des éditions Casterman) lui ont déjà ouvert leurs colonnes. Les grandes maisons d’éditions ne seront pas en reste, et fil des ans Golo publiera ses album chez Albin Michel, Dargaud, Futuropolis, Casterman ou plus tard L’Association.

Dans une même veine que la _Ballade_, une sorte de rencontre du _comix_ et du néo-polar, Golo produira avec Frank plusieurs album qui sont pour certains des références incontournables de l’histoire la bande-dessinée française : _Same player shoots again_ (1982)_, Le Bonheur dans le crime_ (1982)_, Nouvelles du front_ (1985), _La Variante du dragon_ (1989).

![](/uploads/image-004053.jpg)

A partir du début des années 1990, il s’engage dans une carrière en solo et s’éloigne en même temps des rivages du polar. Il réalise ainsi une adaptation brillante du roman philosophique _Mendiants et Orgueilleux_ (Casterman, 1991) du grand écrivain francophone égyptien Albert Cossery, que l’on redécouvre alors en France.

Le roman se passe au Caire, en Egypte, pays qui attire et charme Golo, et où il se rend régulièrement depuis le milieu des années 1970. Au point qu’il finira par s’y installer. Il dessine même, depuis 1993 pour la presse locale et expose parfois dans des galeries. Après l’adaptation du roman le plus célèbre de Cossery (il y a même une adaptation cinématographique de Jacques Poitrenaud de 1972), il décide de s’établir pour de bon au Caire.

Une histoire d’amour pour cette ville et ce pays que Golo partagera avec le lecteur sous forme d’un récit très personnel, à la manière d’un carnet de notes et croquis de voyage composé d’anecdotes et de scènes de vie instantanées, dans les deux volumes de _Mes mille et une nuits au Caire_ publiée par Futuropolis (2009 et 2010).

Passion _cairote_ récurrente dans son travail, avec _Les Couleurs de l'infamie_ (Dargaud, 2003), une seconde adaptation d’un roman d’Albert Cossery, ou les deux volumes des _Carnets du Caire_ (Les Rêveurs, 2004 et 2006).

Mais sans doute, au dessus de toute autre chose, la passion de Golo, comme n’importe quel auteur, est probablement celle de la narration. Raconter des histoires, faire vivre des personnages, fabriquer du sens et des émotions, avec des mots et des dessins, bien au-delà des cases et des bulles. Et Golo est un véritable maître dans l’art du roman graphique.

S’il fallait encore s’en convaincre, il suffirait de se plonger dans son _B. Traven, portrait d'un anonyme célèbre_ (Futuropolis, 2007), sur les traces de ce soldat perdu libertaire de la littérature allemande, ou encore dans les deux tomes de son _Istrati_ (Acte Sud BD, 2017 et 2018) autour de la vie et de l’œuvre de l’écrivain roumain de langue française Panait Istrati.

Il suffit de laisser opérer la magie du récit.

**Aris Papathéodorou**

![](/uploads/image-004054.jpg)

***

_Des dessins de Golo seront exposés, du 16 au 19 août, en Salle des expositions de la mairie. Une rencontre avec l’auteur, animée par Martine Deguillaume, est organisée le 18 août de 11h à 12h30, toujours en salle des expositions._
