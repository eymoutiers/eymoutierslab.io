---
title: "Bienvenue sur le site des Ecrits d’août"
date: 2019-07-01T04:57:40.000+00:00
description: "Voici donc le site Web des rencontres où vous pourrez trouver toutes les informations sur le invités, le programme, les lieux d’Eymoutiers investis, etc."
featured_image: "photo-1489389944381-3471b5b30f04-2048.jpeg"
categories:
- Infos

---
A l’arrache. Comme il se doit...

Voici donc le site Web des rencontres **Les écrits d’août**, où vous pourrez trouver toutes les informations utiles et nécessaires sur les invités, le programme de chacune des quatre journées, les lieux d’Eymoutiers investis, etc. Tout cela sera complété et enrichi comme il se doit dans les jours qui viennent.

Quelques points de passages obligés sont d’ors et déjà disponibles, en particulier :

* [Le programme, en l’état de l’art](http://ecrits-aout.fr/documents/le-programme/).
* [Une présentation des auteurs et auteures](http://ecrits-aout.fr/documents/les-auteurs/).

Vous pouvez aussi suivre l’actualité des rencontres **Les écrits d’août** sur les réseaux dits sociaux : [Twitter](https://twitter.com/ecrits_aout) et [Mastodon](https://framapiaf.org/@ecrits_aout), et sur [Facebook](https://www.facebook.com/Festival-Les-%C3%A9crits-dao%C3%BBt-Eymoutiers-Du-16-au-19-ao%C3%BBt-2019-2438129776424126/) aussi. Celles et ceux qui préfèrent utiliser les [flux RSS](http://ecrits-aout.fr/articles/index.xml) ne sont pas oubliés non plus. Il y en a pour tous les goûts.

_That’s All Folks!_  
Du moins pour l’instant

----
<p class="text-muted">
Photo de Markus Spiske (<a href="https://unsplash.com/photos/8OyKWQgBsKQ">Unsplash</a>)
</p>
