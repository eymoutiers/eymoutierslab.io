---
date: 2019-08-17T08:42:01.000+00:00
title: Les poupées et la cabane à littérature de Sophie Roussel
featured_image: "/photo-20190817-sophie-roussel-2048.jpg"
description: En conclusion du festival sera joué le spectacle “Petites natures” proposé
  par Sophie Roussel, mêlant art contemporain et littérature
categories:
- Programme
- Infos

---
_En conclusion du festival et dans le cadre de la « Journée des luttes », sera joué le spectacle « Petites natures » proposé par Sophie Roussel, mêlant art contemporain et littérature. Nous reprenons ici l’article publié par notre confrère_ [_Les Echos_](http://www.l-echo.info/article/haute-vienne/2019-08-16/eymoutiers-marionnettes-pour-parler-litterature-69946.html)_._

La comédienne déroule son histoire autour d’une cabane en osier et de poupées créées à partir de matériaux de récupération (pierre, bois, et toute chose issue de la nature) selon la technique des Indiens Hopi avec leurs _kachinas_. Elle conte l’histoire des livres et de la vie. C’est un spectacle à cœur ouvert sur la littérature, mais aussi la découverte d’une personnalité riche de sens, qui donne toute sa force à ce moment inclassable.

«Ce spectacle est né d’une colère, lorsque j’ai entendu pour la énième fois le mot “impacter” à la radio. Ce mot n’existe pas ! Il existe près de 20 verbes pouvant le remplacer !», explique-t-elle. La solution, face à cette colère, a été de créer des poupées, combinant bois, pierre, mousse, feuilles, plumes… et d’associer à chacune d’elle une phrase issue d’œuvres littéraires, romans ou poèmes afin d’en révéler la force.

Elle n’a pas choisi ces pépites littéraires en fonction de l’importance ou de la notoriété de l’écrivain, mais pour la dynamique de la phrase : «Je suis fascinée par la phrase, C’est une unité de langage à taille humaine. Car lire est difficile : qui a lu les 8000 pages de _A la recherche du temps perdu_ de Proust ?! Il faut être décomplexé par rapport à la littérature qu’on érige sans arrêt comme un “monument” de moins en moins visité alors que chacun devrait pouvoir se frayer un chemin dans la vaste et dense forêt littéraire. »

Ce spectacle répond au pari que l’artiste s'était lancé : « Lorsque vous en ressortirez, vous aurez ri, aurez été émus, aurez réfléchi, vous vous sentirez guéris du spleen de Baudelaire, tout en vous étant cultivés l’air de rien. Vous aurez appris et compris. »

Un _one woman show_ à la littéraire, profond, sensible, faisant des faiblesses une force… Sophie Roussel est elle-même une de ces « petites natures », à l’image de laquelle on aimerait être, qui donne envie de (re)lire et même d’écrire ou de créer. Un spectacle rafraîchissant et inclassable.

**_Les Echos_** -- Rédaction régionale

***

**Infos pratiques** : lundi 19 août à 21h à la Font-Macaire à Eymoutiers (pas de stationnement sur place ; venir à pied depuis le centre en empruntant la route de Nedde/rue Farges puis le chemin juste avant le pont,). Spectacle gratuit. Durée 1h05.