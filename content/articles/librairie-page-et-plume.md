---
date: 2019-08-14T23:03:53+00:00
title: Grâce soient rendus à la Page et la Plume
featured_image: "/photo-3540348886486876160-page-et-plume.jpg"
description: Grâce à la librairie indépendante “Page et Plume” de Limoges il y aura
  une belle table de livres aux Écrits d’août
categories:
- Pratique
- Infos

---
**« Page et Plume »**, à Limoges, est une de ces librairies que le monde nous envie. Je ne plaisante pas -- la survie d'un réseau de bonnes librairies comme il en existe en France est une rareté dans le monde.

Sébastien, Aurélie et les autres, outre le bon boulot classique de libraires conseilleurs en lecture, assument aussi un rôle de diffuseur pour toutes **les petites librairies** de la région limougeaude qui ne peuvent se permettre de passer par « l’office », ce système abusif dans lequel les éditeurs envoient d’office leurs parutions aux libraires en les leur facturant. Et même s'ils les remboursent en cas d'invendus, cela revient à demander à la librairie de faire une avance de trésorerie à l’édition.

![](/uploads/photo-8244840322023030784.jpg)

Comme l'illustre la photo ci-jointe, prise lors d’un débat que j'eus avec notre chère Dominique Manotti, cela concerne une bonne douzaines de librairies du coin. C’est grâce à « Page et Plume » que des petites librairies comme notre chère « **Passe-Temps »** à Eymoutiers survivent, et c’est grâce à « Page et Plume » qu'il y aura une belle table de librairie aux **Écrits d’août**.

Grâce soient rendus à la Page et la Plume !

**Serge Quadruppani**

***

La librairie est située dans le centre de Limoges, entre le Tribunal de Commerce et les Halles.

Page et Plume  
4, place Motte -- 87000 Limoges  
Tél. : [+33 555346273](tel:+33555346273)

***

Liens

* [Page et plume Web)](https://www.page-et-plume.fr/ "Site vitrine")
* [Page et plume (Facebook)](https://www.facebook.com/pageetplume/ "Facebook")