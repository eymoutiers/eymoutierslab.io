---
title: "Point de départ : une idée de rencontres"
description: "Une vingtaine d’auteurs invités pour participer chacun à un temps
  consacré à leur travail et à leurs centres d’intérêt"
date: 2019-06-17T04:57:40.000+00:00
featured_image: "photo-1197-rencontres-2048.jpg"
categories:
- Infos
- Organisation
---
**_Une première présentation de ce qui n’était encore qu’un projet de rencontres littéraires à Eymoutiers. Une simple idée qui depuis à fait son chemin. La présentation définitive des rencontres est disponible_** [**_ici_**](http://ecrits-aout.fr/documents/le-projet/)**_._**

L’arrivée d’une troupe de théâtre de haut niveau (celle de Sylvain Creuzevault, installé dans les anciens abattoirs) à Eymoutiers, et le festival « Le théâtre rate », inventé dans l’urgence et particulièrement réussi (en qualité comme en affluence) au mois d’août 2018, donnent une impulsion supplémentaire aux énergies culturelles déjà présentes dans cette ville d’un peu plus de 2000 habitants, située au bord du plateau de Millevaches.

C’est dans ce cadre et dans une continuité évidente avec la deuxième édition du festival de théâtre que **Serge Quadruppani**, écrivain, traducteur et directeur de collection, lance, avec la complicité de **Jérôme Leroy**, écrivain, et d’**Anne Echenoz**, chargée de communication dans le secteur culturel, des journées de rencontres littéraires à Eymoutiers. L’idée est d’être à complet contretemps puisque ces rencontres se dérouleront dans la deuxième quinzaine d’août 2019, à un moment où les festivals littéraires font généralement relâche, et en rupture avec cette manie bien française des alignements de tables à dédicaces.

Quinze auteurs sont ainsi invités, pour participer chacun à un temps consacré à leur travail et à leurs centres d’intérêt – pas tous forcément littéraires : ils peuvent être aussi politiques, culinaires, cinématographiques, etc. Une présentation et un entretien avec chaque auteur seront suivis de lectures d’extraits de son œuvre, avec éventuellement accompagnements musicaux et/ou projections. Un des temps forts de ces rencontres sera notamment une « joute de traduction ».

----
<p class="text-muted">
Photo d’Anne Echenoz
</p>
