---
title: "Un bon début... avec Nicolas Fargues"
date: 2019-07-30T04:57:40.000+00:00
description: "Le vendredi 16 août 2019, place des Coopérateurs à Eymoutiers, les Écrits d’août débuteront par une rencontre avec Nicolas Fargues."
featured_image: "photo-9292095-nicolas-fargues.jpeg"
categories:
- Infos
- Programme
---
Le vendredi 16 août 2019, place des Coopérateurs à Eymoutiers, les Ecrits d’août débuteront par une rencontre avec **Nicolas Fargues**. Lecture par **Marc-Henri Lamande** d’extraits de _Attache le cœur_, dialogue avec Serge Quadruppani. Beau début pour une journée intitulée « Le jour de l’Etranger ».

> Ici, la galère, la vraie, tu fais avec. Les galères de transport, de job et de dot, les galères d’un peu tout et n’importe quoi, tu fais avec. Ton avenir aussi boiteux que la qualité du courant fourni par la compagnie nationale d’électricité, tu fais avec. Les Blancs nous plaignent : Mais comment pouvez-vous vivre dans des conditions pareilles ? Ce désastre qui n’en finit plus, avez-vous vraiment la volonté d’y mettre un terme ? Et moi je dis que laisse, mon ami, laisse. Ce n’est pas avec des si, des il faut que et des voilà comment qu’on met Yaoundé dans une bouteille de J&B.

Nicolas Fargues est né en 1972 à Meulan. Il a passé son enfance au Cameroun, au Liban puis en Corse. Il a fait des études de lettres à la Sorbonne à Paris. Deux ans de coopération en Indonésie. Retour à Paris, petits boulots, puis publication en 2000 de son premier roman _Le Tour du propriétaire_. Depuis 2002, il a vécu à Diego-Suarez, Yaoundé et Wellington et a publié une douzaine de livres parmi lesquels _Rade Terminus_, _J’étais derrière toi_, _Beau rôle_, _Je ne suis pas une héroïne_.

Son nouveau livre est un recueil de 15 nouvelles intitulé _Attache le cœur_ paru aux éditions P.O.L

**[Nicolas Fargues, de retour au Cameroun](http://www.rfi.fr/emission/20190208-fargues-nicolas-ecrivain-francais-retour-cameroun)**   
_Emission du 8 février 2019 sur RFI à (ré)écouter_

----
<p class="text-muted">
Photo RTS.ch
</p>
