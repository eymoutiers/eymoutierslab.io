---
title: "Rendez-vous le 2 août"
date: 2019-07-27T04:57:40.000+00:00
description: "Nous organisons une réunion pour présenter le festival aux Pelauds, aux habitants du plateau et au reste de l’univers le vendredi 2 août..."
featured_image: "photo-1540380835319-cb692a9421b3-2048.jpeg"
categories:
- Infos
- Organisation
---
Nous organisons une réunion destinée à présenter le festival aux Pelauds, aux habitants du plateau et au reste de l’univers (ainsi qu’aux journalistes) le **vendredi 2 août** à 18 heures dans la salle de réunion de la mairie.

Ce sera pour nous l’occasion d’expliquer l’esprit général de ce que nous souhaitons être une première édition, en attendant bien d’autres...

Nous serons aussi preneurs de toute remarque, suggestion et proposition de coup de main (nous sommes une vingtaine dans l’association **Les écrits d’août**). Des membres de la municipalité, qui soutient solidement le projet, seront aussi présents.

### Une surprise

Frédéric Lordon nous fera le plaisir de sa présence parmi nous pendant les rencontres, en **invité surprise**, et donc non inscrit dans le programme, mais on lui fera de la place.

* [Les auteurs et auteures](http://ecrits-aout.fr/documents/les-auteurs/).
* [Le programme](http://ecrits-aout.fr/documents/le-programme/).

### Un rappel

Vous pouvez suivre l’actualité des rencontres **Les écrits d’août** sur ce blog et sur les réseaux dits sociaux : [Twitter](https://twitter.com/ecrits_aout) et [Mastodon](https://framapiaf.org/@ecrits_aout), et [Facebook](https://www.facebook.com/Festival-Les-%C3%A9crits-dao%C3%BBt-Eymoutiers-Du-16-au-19-ao%C3%BBt-2019-2438129776424126/). Celles et ceux qui préfèrent utiliser les [flux RSS](http://ecrits-aout.fr/articles/index.xml) ne sont pas oubliés non plus.

----
<p class="text-muted">
Photo de Louis Maniquet (<a href="https://unsplash.com/photos/_r9i4_j4a8o">Unsplash</a>)
</p>
