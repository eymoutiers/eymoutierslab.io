---
date: 2019-08-11T04:57:40.000+00:00
title: "Résistance, résistances"
featured_image: "/photo-97055f3fbc7d4528-gilles-perrault-1.jpg"
description: "Gilles Perrault nous fera le plaisir d’être présent pour nous parler
  de son Histoire amoureuse de la Résistance."
categories:
- Programme
- infos

---
Notre très cher ami Gilles Perrault nous fera le plaisir très grand d’être présent en compagnie de son épouse, la très tonique et indispensable Thérèse, durant tout le festival des Écrits d’Août.

Il sera au centre de la table ronde finale, avec son _Dictionnaire amoureux de la Résistance_ (Plon, 2014). Que ce soit dans ses bouquins sur le Maroc d’Hassan II (_Notre ami le roi_, qui lui valut d’avoir quelques années un « contrat » sur sa tête et provoqua une crise diplomatique), sur Leopold Trepper (_L’Orchestre Rouge_), sur Henri Curiel (_Un homme à part_), ou dans ses nombreux livres sur la Seconde Guerre mondiale, ce journaliste et romancier qui n’a cessé de dénoncer l’injustice de la Justice (_Le Pull-Over Rouge_), qui sut dès son premier ouvrage montrer la similitude entre l’entraînement des paras et celui des SS (_Les Parachutistes_), n’a jamais cessé de nous parler de résistances.

Mais la Résistance, celle qu’enfant il admirait en la personne de ses parents qui abritaient à la maison des parachutistes anglais, a toujours été la boussole de sa pensée et de son éthique politique. Dans son dictionnaire amoureux, il nous parle avec le talent de conteur qu’on lui connaît, à travers des portraits de résistants et d’innombrables rubriques aussi diverses que « Vélos » ou « Chefs », des mille facettes de ce phénomène si singulier: comment dans un pays qui s’était vautré dans la honte du vichysme et de la collaboration, des femmes et des hommes de tous milieux et de tous horizons politiques, ont su sauver l’honneur, infliger à l’ennemi de dures pertes et, c’est un des chapitres les plus bouleversants du livre, trouver face à l’horreur de la répression et de la mort, une forme de bonheur.

On compte sur Jérôme Leroy, notre écrivain-résident qui le présentera, ainsi que sur Alain Damasio, qui a tant travaillé sur les résistances au contrôle social, et sur David Dufresne, qui a suivi au plus près de ses pertes le mouvement des Gilets Jaunes, pour nous faire sentir les résonances entre la Résistance et les résistances d’aujourd’hui.

**Serge Quadruppani**
