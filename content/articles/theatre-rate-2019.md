---
title: "Entre Théâtre rate et Écrits d’août"
date: 2019-08-08T04:57:40.000+00:00
description: "Le Festival Le théâtre rate, qui aura lieu à Eymoutiers du 9 au 14 août, est une sorte de grand frère pour Les Écrits d’Août..."
featured_image: "photo-515223797700.jpg"
categories:
- Infos
- Amis
---

Pour la seconde année consécutive Eymoutiers accueille le festival **Le théâtre rate**, qui aura lieu du 9 au 14 août. Derrière ce projet, la compagnie de théâtre « Le Singe », menée par **Sylvain Creuzevault**, une dizaine de personnes qui se sont installés à Eymoutiers… où ils et elles réhabilitent les anciens abattoirs. Une manifestation qui est une sorte de grand frère pour les **Écrits d’Août**.

> Après le Théâtre qui rate (et qui fut, l’année dernière une impressionnante réussite), nous avons, Anne et moi, voulu faire appel aux Écrits qui doutent (ou aux Écrits doux, ça dépend de la prononciation). C’est vrai que par les temps qui courent, il y a beaucoup de raisons de douter (par exemple de la démocratie et de sa police). Pour nourrir vos doutes et vos douceurs, un seul choix au mois d’août: venez à Eymoutiers du 9 au 19 août. Et merci à Sylvain, Élodie, Flora, Frédéric et tous les autres qui nous ont donné l’impulsion initiale… **Serge Quadruppani**

L’arrivée d’une troupe de théâtre de haut niveau à Eymoutiers, et le festival **Le théâtre rate**, inventé l’an passé dans l’urgence et particulièrement réussi (en qualité comme en affluence), donnent une impulsion supplémentaire aux énergies culturelles déjà présentes dans cette ville d’un peu plus de 2000 habitants, située au bord du plateau de Millevaches.

Informations sur le festival « Le théâtre rate »    
[https://www.facebook.com/TheatreRate/](https://www.facebook.com/TheatreRate/)

----
<p class="text-muted">
Photo de Cécile Descubes (France 3)
</p>
