---
date: 2019-08-21T06:41:36+00:00
title: Toutes nos espérances...
featured_image: "/photo-6453337002468179968.jpg"
description:  "Le festival a dépassé toutes nos espérances par l’affluence impressionnante, par la densité et la diversité des débats. Difficile de détacher des moments marquants..."
categories:
- Compte-rendus
---
Le festival **Les Écrits d’août** a dépassé toutes nos espérances par l’affluence impressionnante, par la densité et la diversité des débats. Difficile de détacher des moments marquants, ils l’ont tous été. Difficile, mais on va essayer quand même, de manière purement subjective.

A vous qui avez tout vu d’ajouter vos propres moments. La rencontre initiale avec Nicolas Fargues a marqué les esprits par sa qualité, qu’il s’agisse des propos de l’intervenant (sur l’écriture blanche et la réalité noire) de l’écoute du public très nombreux.

Les banquets littéraires ont été une belle réussite, sinon pour les finances (faudra revoir ça) au moins pour le plaisir de l’échange et pour la disponibilité (à une exception près) des auteurs à y intervenir : de manière purement subjective, disons qu’il nous revient en particulier celles de Karine Parrot et de Victor Collet au banquet de Montagne Accueil Solidarité, et celles de Jérôme Leroy et de Hervé Le Corre au banquet Gilets Jaunes.

Il était assez étonnant de voir comment plus de 70 personnes ont pu participer avec passion à la rencontre sur la traduction, avec Hervé Denès et Béatrice Roudet-Marçu, en proposant leurs propres solution sur un texte ardu: on gagne toujours à miser sur l’intelligence.

Le performance d’Eric Vuillard et de Julien Boudart fut aussi réussie que celle de Sophie Roussel, et ce n’est pas peu dire. La rencontre avec Hervé Le Corre, animée par les questions futées de Pascal Léonard et de Daniel Couégnas, et les chansons de Jacques et Cathy… Le moment polar avec Pagan, Topin et Pelletier, le moment Gilets jaunes avec les amis Alain Damasio et David Dufresne, le meeting final avec ces deux-là plus Frédéric Lordon…

En définitive, on se retrouve à relire le programme dans sa totalité… Et n’oublions pas le concert de Jaap Mudler, avec la mise en musique d’un poème de Jérôme Leroy et de ceux de Vissotsky, instant intense s’il en fut…

**Serge Quadruppani**
