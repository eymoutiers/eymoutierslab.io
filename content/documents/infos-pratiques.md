---
title: 'Pratique : accéder, séjourner à Eymoutiers'
date: 2019-08-04T04:57:40.000+00:00
description: Quelques informations pratiques
featured_image: photo-8946400185_5936093eb6.jpg
weight: '040'

---
**Comment accéder à Eymoutiers**

Par train : gare d’Eymoutiers-Vassivière, à 3/4 d’heure de train de Limoges (ligne Limoges terminus Eymoutiers ou Meymac ou Ussel)

En voiture : Eymoutiers est situé sur la D979, à 50 minutes de Limoges

- En venant du Nord (Paris). A20 jusqu’à Limoges, puis sortie 35 « Feytiat » ou sortie 34 « Lac de Vassivière ».

- En venant du Sud (Toulouse). A20 jusqu’à la sortie 42 Saint-Germain les Belles. A Saint-Germain les Belles, prendre la direction de Châteauneuf la Forêt (par D16 puis D15) et suivre la direction d’Eymoutiers (par la D979).

- En venant de l’Est (Clermont-Ferrand). Direction Aubusson (par la D941), suivre Bourganeuf (D941) puis Peyrat le Château (D940) et enfin Eymoutiers.

- En venant du Sud-Ouest (Bordeaux). Brive A89, puis Limoges A20.

- En venant du Nord-Ouest (Poitiers). Limoges (N147).

**Hébergement**

Camping municipal à prix modique (7 euros par personne par jour).

Pour les gîtes et les hôtels il y a du choix. Le plus simple est de contacter l’Office du Tourisme des Portes de Vassivière (Tél. : 05 55 69 27 81). Plus d’infos sur [tourisme-eymoutiers.fr](http://tourisme-eymoutiers.fr)

----
<p class="text-muted">
Photo de Christian Lapie (<a href="https://www.flickr.com/photos/56025300@N05/">Office de Tourisme des Portes de Vassivière</a>)
</p>
