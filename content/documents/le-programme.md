---
title: Le programme
date: 2019-07-04T04:57:40.000+00:00
description: Programme complet des rencontres “Les écrit d’août”, du 16 au 19 août
  à Eymoutiers
featured_image: photo-IqBUhDpAaA2RMoPlxM8nFA-spectacle.jpeg
weight: '020'

---
Vous pouvez désormais télécharger un fichier au [format PDF](https://ecrits-aout.fr/uploads/pdf/programme-ecrits-d-aout-2019.pdf) du programme.


<h2><small>Vendredi 16 août 2019 </small>
<br><span class="highlight">Le jour de l’Étranger</span></h2>
<br>

#### Aller chez l’étranger…

**11 h - 13 h** : rencontre avec Nicolas Fargues autour de son dernier livre, <em>Attache le cœur</em>. Présentation par Serge Quadruppani Lecture par Marc-Henri Lamande.  
Lieu : Place des coopérateurs

#### Accueillir l’étranger…

**13 h - 15 h** : banquet préparé avec l’association Montagne Accueil Solidarité et rencontre avec Karine Parrot autour de son livre <em>Carte blanche. L’État contre les étrangers</em> ; Victor Collet, auteur de <em>Nanterre, du Bidonville à la cité</em> ; Nicolas Fargues (cf. plus haut).  
Lieu : les arcades du Foyer de la mairie  
Sur réservation avant le 10 août par sms au : 06 30 14 31 32, places limitées, prix libres

**15h30 - 17 h** : des émigrés d’hier aux exilés d’aujourd’hui : table ronde avec Karine Parrot,Victor Collet et un-e représentant-e de Montagne Accueil Solidarité

#### Lire l’étranger…

**17h30 – 19h30** : « Moi je l’aurais plutôt dit comme ça ». joute de traduction avec Béatrice Roudet-Marçu et Hervé Denès.

Béatrice Roudet-Marçu et Hervé Denès se pencheront sur un court texte, inédit en français, d’un auteur anglophone. Chacun aura préparé sa traduction à l’avance et viendra la confronter à celle de son collègue. Devant un écran présentant le texte original encadré des deux traductions, l’une et l’autre défendront, bien entendu amicalement, leurs choix de traduction et accueilleront volontiers les questions et suggestions du public.

#### Ecouter l’étranger…

**21 h** : concert de Jaap Mulder sur un poème de Jérôme Leroy et autour du poète russe Vladimir Vissotski.  
Lieu : Salle des expositions de la mairie

----

<h2><small>Samedi 17 août 2019</small>  <br>
<span class="highlight">Le jour de la cuisine</span><br>
<small>la petite cuisine de la télé, celle de la police et la grande cuisine populaire</small></small></h2>
<br>

<em>Journée avec Hugues Pagan, Chantal Pelletier et Tito Topin (et Serge Quadruppani en auteur bellevillois)</em>

**10 h 30** : projection d’un épisode du « Commissaire Navarro », série créée par Tito Topin, de « Police district », série créée par Hugues Pagan, ainsi que « Tirez sur le caviste » tiré d’un court roman de Chantal Pelletier.  
Lieu : Salle des expositions de la mairie

#### Les fourneaux de Chantal

**12 h - 15 h** : banquet préparé avec Chantal Pelletier (son dernier livre paru en mai dans la Série noire s’intitule <em>Nos derniers festins</em>).  
Lieux : Sous les arcades du foyer de la mairie  
Sur réservation avant le 10 août par sms au : 06 20 91 83 40, places limitées, prix libre

#### Trois costauds cuistots du Noir
<em>Présentés par Serge Quadruppani</em>

**15h30** : Tito Topin, les romans du Maroc Présentation des romans qui ont pour cadre le Maroc de la décolonisation : <em>55 de fièvre, Photo-finish, Les Enfants perdus de Casablanca</em>

**16h** : Hugues Pagan, ou « Comment peut-on être flic ? » Hugues Pagan s’interroge et nous interroge sur le métier de policier qu’il a pratiqué dans une autre vie, et sur l’adjectif « policier » apposé à un certain type de romans.

**17h30** : Chantal Pelletier parlera de sa collection « Exquis d’écrivains ».   
Lieux : place des coopérateurs

**18h -19h** : le polar, le réel, la télé : à quelle sauce la télévision cuisine-t-elle la réalité du métier de policier et des tensions sociales qu’il doit régler ? Avec Chantal, Tito et Hugues, animé par Serge Quadruppani et tous les auteurs qui voudront intervenir.  
Lieu : place des coopérateurs

#### Le chaudron bellevillois

**20h30** : projection du film <em>Belleville est un roman</em>. Réalisation José Reynes sur une idée de Serge Quadruppani, 1996, 72 minutes.    
Lieu : Cinéma Le Jean-Gabin

----

<h2><small>Dimanche 18 août 2019 <br></small>
<span class="highlight">Le jour des auteurs</span></h2>
<br>

**11h - 12h30** : rencontre avec Golo, dessinateur de bandes dessinées, et inauguration de son exposition qui durera du 16 au 19 août, animée par Martine Deguillaume.  
Lieu : Salle des expositions de la mairie

**13h - 15h** : banquet des auteurs : repas préparé sur les indications des auteurs (quelques-uns des plats qu’ils auront suggérés) et présentation par chacun d’un livre dont il n’est pas l’auteur et qu’il a aimé.   
Lieu : Sous les arcades du foyer de la mairie   
Sur réservation avant le 10 août par sms au : 06 12 89 03 81, places limitées, prix libres

**17h - 19h30** : présentation de l’œuvre d’Eric Vuillard par Pascal Léonard. Création littéraire et musicale de Julien Boudart et Eric Vuillard.  
Lieu : Cour du château Font Macaire

**21h** : Une heure avec Valère Novarina. Messager : Marc-Henri Lamande.  
Lieu : parvis de la Collégiale

----

<h2><small>Lundi 19 août 2019</small> <br>
<span class="highlight">Le jour des luttes</span></h2>
<br>

**11h - 13h** : Hervé Le Corre. Présentation de l’œuvre par Daniel Couégnas. Jacques Nony et Cathy Tostivint liront des extraits de son dernier livre <em>Dans l’ombre du brasier</em> et chanteront des chansons de la Commune.  
Lieu : place des coopérateurs

**13 h - 15 h** : banquet des luttes, organisé par les gilets jaunes de la montagne limousine, en présence de David Dufresne et Alain Damasio. _Repas partagé, chacun apporte un plat ou une boisson de préférence de couleur jaune !_  
Lieu : Sous les arcades du foyer de la mairie

#### De la difficulté des formes de vie non conformes

**15h30 - 17h** : rencontre avec Marin Ledun autour de son livre _Salut à toi, ô mon frère_, lecture par Marc-Henri Lamande.   
Lieu : Place des coopérateurs

**17h30** : carte blanche à David Dufresne et Alain Damasio.  
Lieu : place des Coopérateurs

**19 h 30** : rencontre avec Gilles Perrault, animée par Jérôme Leroy sur le thème « Résistance, résistances », en lien avec l’université populaire d’Eymoutiers, avec des interventions d’Alain Damasio et de David Dufresne.  
Lieu : Salle des expositions de la mairie

**A partir de 21h** : présentation de « Petites natures. L’orphelinat de la phrase et ses poupées gardiennes » par Sophie Roussel, lectures d’extraits de _Lou, après tout_ de Jérôme Leroy, grillades et musique, harangues et chansons.   
Lieu : Cour du château Font Macaire
