---
title: Le projet
date: 2019-07-05T04:57:40.000+00:00
description: Les écrits d’août, des rencontres littéraires qui investissent Eymoutiers
featured_image: photo-1197-rencontres-2048.jpg
weight: '010'

---
_Une vingtaine d’auteures et d’auteurs invités. Une présentation et un entretien avec chacun, entrecoupés de lectures d’extraits de son œuvre, avec éventuellement accompagnements musicaux et/ou projections._

Temps forts de ces rencontres dont chaque jour sera consacré à un thème : un banquet littéraire quotidien sur le thème du jour, une « joute de traduction » pour découvrir les coulisses du métier de traducteur, des projections de séries télés et un film sur Belleville et ses romanciers (réalisé par José Reynes, sur une idée de Serge Quadruppani), une exposition de Golo, un concert de Jaap Mulder sur des poèmes de Jérôme Leroy et du poète russe Vladimir Vyssotski, une création musico-littéraire d’Eric Vuillard (Goncourt 2018) et Julien Boudard. Les auteurs parleront de cuisine (Chantal Pelletier), de polar à la télé (Chantal, Tito Topin et Hugues Pagan), du droit des étrangers (Karine Parrot) et de la vie au Cameroun (Nicolas Fargues), de la Commune de Paris (Hervé Le Corre), d’une famille anar confrontée à la laideur du monde (Marin Ledun) ou de la dernière bataille de la Douceur à Eymoutiers (Jérôme Leroy), des bidonvilles de Nanterre (Victor Collet) et enfin dans un dialogue entre notre grand ancien Gilles Perrault, Jérôme Leroy notre auteur-résident de cette année, et la salle, on échangera sur le thème de la Résistance hier et des résistances aujourd’hui.

Pas de dédicaces obligatoires mais des échanges impromptus avec les auteurs au hasard des événements, des repas autour d’une vaste table où on trouvera aussi bien les livres des invités ainsi que d’autres qu’ils nous auront suggérés, ainsi que la production de petites maisons d’édition invitées (Libertalia, l’Insomniaque…).
