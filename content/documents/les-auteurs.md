---
title: "Les auteurs"
date: 2019-06-18T06:57:40.000+00:00
description: "Présentation des auteurs et auteures qui seront présent à cette première
  édition des rencontres d’Eymoutiers"
featured_image: "photo-19214141-2019-dada.jpg"
weight: '030'

---
_Une vingtaine d’auteurs  et d’auteures réunis pour des rencontres littéraires à Eymoutiers_

#### Victor Collet

Victor Collet a vécu, mené ses recherches et milité dix ans à Nanterre, des luttes de quartier à la défense des étrangers et auprès des habitants des bidonvilles d’aujourd’hui. Son étude _Nanterre, du bidonville à la cité_ est paru chez Agone en mars 2019.

#### Alain Damasio

Auteur de trois romans phares de la science-fiction contemporaine, _La Zone du Dehors_, _La Horde du Contrevent_ et le tout récent _Les Furtifs_, cet auteur a su toucher un vaste public en extrapolant les tendances les plus dangereuses de nos sociétés (du contrôle généralisé à la privatisation de tout), et les formes de résistance apparues aujourd’hui, dans les ZAD et ailleurs. Son travail de romancier s’accompagne de productions audio-visuelle en collaboration avec des vidéastes et des musiciens.

#### Hervé Denès

Traducteur de l’anglais (_Zelda Popkin, Congés pour meurtre ; Boxcar Bertha, Une autobio-
graphie..._), du chinois (Li Jingze, _Relations secrètes_ ; Xu Zecheng, _La Grande harmonie_...), de l’italien et de l’espagnol, il est aussi l’auteur de deux livres sur la Chine en collaboration avec Charles Reeve et d’un émouvant récit de souvenirs dans la Chine à la veille de la Révolution culturelle : _Douceur de l’aube_ (­L’insomniaque).

#### David Dufresne

Deux fois lauréat du Prix des Assises du journalisme, en 2012, pour _Tarnac, magasin général_, et en 2019 pour son travail sur le décompte des blessures des Gilets Jaunes dans son blog « Allô, place Beauveau », auteur de nombreux ouvrages dont le récent récit de journalisme subjectif _On ne vit qu’une heure. Une virée avec Jacques Brel_ (Seuil, 2018), de documentaires et de webdocumentaires, c’est une figure reconnue et respectée dans un métier si souvent remis en cause.

#### Nicolas Fargues

Nicolas Fargues a publié douze romans aux éditions P.O.L. Enfance au Cameroun, au Liban
puis en Corse, études à Paris. Deux ans de coopération en Indonésie, retour à Paris, petits
boulots, publication en 2000 du _Tour du propriétaire_. Depuis 2002, a vécu à Diego-Suarez,
Yaoundé et Wellington. Ses derniers ouvrages parus sont _Je ne suis pas une héroïne_ (P.O.L, 2018) et _Attache le cœur_ (P.O.L, 2018).

#### Golo

Avec son complice Franck, Golo a signé des albums chez Futuropolis, Dargaud et
Casterman puis a continué seul dans le mensuel _(A Suivre)_ en adaptant _Mendiants
et Orgueilleux_ d’Albert Cossery. Parmi ses ouvrages : _B.Traven, portrait d’un
anonyme célèbre_ (Futuropolis) ; _Mes mille et une nuits au Caire_ (Futuropolis)
; _Chroniques de la nécropole_, en collaboration avec Dibou (Futuropolis),
_Istrati_ (Actes Sud bd).

#### Hervé Le Corre

Hervé Le Corre a reçu le grand prix de littérature policière en 2009, le Prix
Mystère de la Critique 2010 pour _Les Cœurs déchiquetés_ (Rivages Noir, 2012) et
les prix Le Point du Polar européen 2014, Landerneau polar 2014 et Michel-Lebrun
2014 pour _Après la guerre_ (Rivages Thriller, 2014). Ses derniers ouvrages parus
sont _Prendre les loups pour des chiens_ (Rivages Noir, 2017) et _Dans l’ombre du
brasier_ (Rivages, 2019).

#### Marin Ledun

Héritier du néopolar, Marin Ledun pose la question des limites du progrès et de
la maîtrise des corps dans la société industrielle. Ses derniers livres parus
sont _Ils ont voulu nous civiliser_ (Ombres Noires, 2017 ; J’ai Lu, 2018) _Salut
à toi ô mon frère_ (Gallimard série Noire, 2018) et _La vie en Rose_ (Gallimard série Noire, 2019)..

#### Jérôme Leroy

Jérôme Leroy a été professeur de français avant de se consacrer à la
littérature. Il est l’auteur de romans, de nouvelles et de poèmes. Ses livres
mélangent les genres du roman noir, du roman policier et de l’anticipation. Ses
derniers livres parus sont _Un peu tard dans la saison_ (La Table Ronde, 2017) ;
_La petite gauloise_ (La Manufacture de Livres, 2018) ; _Nager vers la Norvège_ (La
Table ronde, 2019).

#### Frédéric Lordon

Chercheur en philosophie au CNRS. Il anime également le blog « La pompe à phynance » sur le site du _Monde diplomatique_. Parmi ses derniers ouvrages parus : _Les Affects de la politique_ (Seuil, 2016), _La condition anarchique_ (Seuil, 2018).

#### Hugues Pagan

Hugues Pagan est romancier et scénariste. Il a exercé différents ­métiers avant
de se lancer dans l’écriture. Pour beaucoup d’amateurs, cet ancien flic est
aussi un des plus grands écrivains français du polar. Ses derniers livres parus
sont _Profil perdu_ (Rivages noir, 2017) et _Mauvaises nouvelles du front_ (Rivages
noir, 2018).

#### Karine Parrot

Karine Parrot est professeure de droit à l’Université de Cergy-Pontoise, membre
du GISTI (Groupe d’information et de soutien des immigré.es). Son livre, _Carte
blanche. L’État contre les étrangers_, est paru à La Fabrique en mars 2019.

#### Chantal Pelletier

Chantal Pelletier navigue des nouvelles aux polars, du théâtre (elle a été une
des Trois Jeanne) aux romans, dont quatre ont été publiés chez Joëlle Losfeld :
_Paradis andalous_, 2007 ; _De bouche à bouches_, 2011 ; _Cinq femmes chinoises_, 2013
; _Et elles croyaient en Jean-Luc Godard_, 2015. Son livre _Nos derniers festins_
est paru à la Série Noire en mai 2019.

#### Gilles Perrault

Gilles Perrault est l’auteur de récits-enquêtes qui ont fait date (_L’Orchestre
rouge, Le Pull-over rouge, Notre ami le Roi_...), de la fresque historique _Le
Secret du roi_, et de romans dont _Le Garçon aux yeux gris_, _Les vacances de
l’oberleutnant von La Rochelle_... Ses derniers livres parus sont _Dictionnaire
amoureux de la Résistance_, Plon/Fayard, 2014 ; _Grand-père_, Le Seuil, 2016 et _La
Justice expliquée à ma petite-fille_, Le Seuil, 2017.

#### Béatrice Roudet-Marçu

Angliciste, traduit des romans d’auteurs africains, anglais, nord-américains (Michiel Heyns, Sue Miller, James Patterson…) Elle traduit également pour la presse et pour des institutions internationales. Après avoir enseigné l’interprétariat à l’Université de Londres, elle est aujourd’hui chargée de cours à l’Ecole de Traduction Littéraire.

#### Serge Quadruppani

Serge Quadruppani est romancier, essayiste, éditeur et traducteur, notamment de
la série des Commissaire Montalbano d’Andrea Camilleri. Auteur d’une quarantaine
de livres, ses derniers ouvrages parus sont _Loups solitaires_ (Métailié, 2017) ;
_Le monde des grands projets et ses ennemis_ (La Découverte, 2018) et _Sur l’île
de Lucifer_ (La Geste, 2018).

#### Tito Topin

Né au Maroc, Tito Topin est écrivain, scénariste, graphiste et illustrateur.
Auteur d’une quarantaine de livres, il a collaboré avec Jean Yanne et a créé la
série Navarro, avec Roger Hanin dans le rôle-titre. Ses derniers livres parus
sont _L’exil des mécréants_ (La manufacture de Livres, 2017) et _55 de fièvre_ (La
manufacture de Livres, 2018).

#### Éric Vuillard

Éric Vuillard est écrivain et cinéaste. Il a réalisé deux films, _L’homme qui
marche_ et _Mateo Falcone_. Il a reçu le prix Franz-Hessel 2012 et le prix
Valery-Larbaud 2013 pour deux récits publiés chez Actes Sud, _La bataille
d’Occident_ et _Congo_, ainsi que le prix Joseph-Kessel 2015 pour _Tristesse de la
terre_, le prix Alexandre Viallate pour _14 juillet_ et le prix Goncourt 2017 pour
_L’ordre du jour_. Son dernier livre paru est _La guerre des pauvres_ (Actes Sud,
2018).

----
<p class="text-muted">
Photo de Man Ray : « Groupe de dadaïstes en 1921 ».
</p>
