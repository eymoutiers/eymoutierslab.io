---
title: "Les lieux des rencontres"
date: 2019-07-03T04:57:40.000+00:00
description: "Petite présentation des lieux d’Eymoutiers qui accueilleront les différents rendez-vous des Écrits d’août"
featured_image: "photo-0001-chateau-font-macaire-2048.jpg"
layout: "single_no_banner"
weight: '050'

---
_Salle des expositions de la mairie, Arcades du foyer de la mairie, place des coopérateurs, parvis de la Collégiale, Belvédère, château Fond Macaire... Les écrits d’août investissent Eymoutiers._

{{< figure src="/uploads/photo-0001-chateau-font-macaire-2048.jpg" caption="Le château Fond Macaire" >}}

{{< figure src="/uploads/photo-0001-place-des-cooperateurs.JPG" caption="La place des coopérateurs" >}}

{{< figure src="/uploads/photo-0001-arcades-mairie-2048.JPG" caption="Les arcades du foyer de la mairie" >}}
