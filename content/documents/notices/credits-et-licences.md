---
title: "Crédits et licences"
date: 2019-07-31T04:57:40.000+00:00
description: "Références des services, logiciels et ressources open source utilisées, etc."
featured_image: "/photo-tSfuLGojT60-unsplash.jpg"
weight: "030"
---
Ce site est hébergé sur les serveurs du service **GitLab.com**. L’interface de publication (CMS) est fournit par la société **Forestry**. Le système de publication utilisé est le logiciel libre **Hugo** (licence MIT). Le flux éditorial et productif utilise le logiciel libre **git** (licence GPLv2). Les polices de caractères _open sources_ sont hébergées par **Google Fonts** (mais il n’y a pas de _trackers_) à l’exeception de la fonte Fork Awesome (licences SIL OFL et MIT) qui est auto-hébergée.

* https://gitlab.com/
* https://forestry.io/
* https://gohugo.io/
* https://git-scm.com/
* https://fonts.google.com/
* https://forkaweso.me/

***

Le design graphique et le développement du site ont été réalisé par Aris Papathéodorou. Il s’agit d’une version largement modifiée du thème **UILite Pro** (licence MIT) par UICardio. Les librairies logicielles utilisées sont : **Bootstrap** v4 (licence MIT) pour la base des feuilles de styles CSS, **JQuery** (licence MIT) comme environnement JavaScript global, **popper.js** (licence MIT), **UILite Portfolio** (licence MIT), **espaceFine.js** (licence WTFPL). Les sources du site elles-mêmes sont accessibles sur un dépôt hébergé par **Gitlab**.

* https://uicard.io/
* https://getbootstrap.com/
* https://jquery.org/
* https://popper.js.org/
* https://gitlab.com/eymoutiers/

***
Open source

Voici la liste, et les liens vers la version originale (en anglais), des différentes licences _open source_ utilisées par les composants de ce site Web.

- [The MIT License](https://opensource.org/licenses/mit-license.html)
- [GNU general public license version2](https://opensource.org/licenses/GPL-2.0)
- [SIL Open Font License ((OFL-1.1)](https://opensource.org/licenses/OFL-1.1)
- [Do What The Fuck You Want To Public License (WTFPL)](http://www.wtfpl.net/about/)
    
***

<p class="text-muted">
Photo de Morgan Basham (<a href="https://unsplash.com/@mpbasham">Unsplash</a>)
</p>
