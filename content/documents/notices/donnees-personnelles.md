---
title: "Protection des données personnelles"
date: 2019-06-18T04:57:40.000+00:00
description: "Informations concernant l’utilisation et la (non) conservation de vos
  données personnelles sur ce site"
featured_image: "photo-V4mNfkDmiX4-unsplash-2048.jpg"
weight: "020"

---
Le site **Les écrits d’août** n’utilise aucun dispositif de _cookies_, ni aucun autre système pour tracer votre navigation sur ses pages, ni sur l’Internet plus globalement. Nous n’utilisons par ailleurs ni base de données, ni services commerciaux tiers d’hébergement (CDN) de contenus ou de ressources.

La connexion au site **Les écrits d’août** se fait de façon _chiffrée_ (via le protocoles HTTPS) de manière à réduire les possibilités de surveiller ou d’intercepter votre activité sur celui-ci (comme les pages consultées ou les fichiers téléchargés).

A noter tout de même :

1- Le serveur sur lequel **Les écrits d’août** est hébergé (Gitlab.io) collecte de son côté, comme tout serveur Web, un _minimum_ de données « techniques » de navigation telles que l’identité de votre fournisseur d’accès (Orange, Free, etc.), l’adresse IP qui vous est allouées, ou encore le logiciel de navigation que vous utilisez. Ces données sont collectées par _tous_ les serveurs Internet et sont nécessaires à leur maintenance.

2- Les liens éventuellement contenus dans les pages du site **Les écrits d’août** peuvent pointer vers des services externes qui pourraient être bien _moins scrupuleux_ que nous ne le sommes en matière de collecte de données personnelles (Youtube, Facebook, Twitter, etc.). Il vous appartient en votre âme et conscience (politique) de décider, ou non, de suivre ces liens.

3- L’utilisation du formulaire de contact présent sur la page d’accueil du site **Les écrits d’août** nous fournit, comme on s’en doute, votre adresse email. Celle-ci sera utilisée _uniquement_ pour répondre à votre demande, elle ne sera en _aucun cas_ conservée dans une base de données, et encore moins revendue à des fins commerciales.

Liens et références :

**Code source du site**  
[https://gitlab.com/eymoutiers/eymoutiers.gitlab.io](https://gitlab.com/eymoutiers/eymoutiers.gitlab.io)

**Privacy Badger**  
_Extension pour les navigateurs Firerfox, Chrome et Opera_  
[https://www.eff.org/privacybadger](https://www.eff.org/privacybadger)

***

Version 0.2 – 11-08-2019

***

<p class="text-muted"> Photo de Arvin Keynes (<a href="https://unsplash.com/@arvinkeynes)">Unsplash</a>)
</p>
