---
title: "Mentions légales"
date: 2019-07-31T04:57:40.000+00:00
description: "Force doit rester à la loi: éditeur, hébergeur, et autres mentions légales."
featured_image: "/photo-1535284366524-b78118e721ba.jpeg"
weight: "010"

---
**Editeur**  
Les écrits d’août  
1, chemin des Aubépines  
87120 Eymoutiers  
Tél. : 06 30 14 31 32  
[https://ecrits-aout.fr](https://ecrits-aout.fr)

**Hébergeur**    
GitLab Inc.  
268 Bush Street Suite 350  
San Francisco, CA 94104  
Etats-unis  
Tél. : 1-415-829-2854  
[https://about.gitlab.com](https://about.gitlab.com)

***
**Les écrits d’août Eymoutiers**  
_Association régie par la loi 1901 déclarée le 2 mars 2019 en préfecture de la Haute-Vienne_  
Objet : promouvoir et organiser tous événements culturels autour de l’écrit (fiction, poésie, document, journalisme, essais) à Eymoutiers (Haute-Vienne), que ce soit pendant la manifestation « les Écrits d’août » ou durant toute l’année, notamment à travers des rencontres et une ou plusieurs résidences d’auteur.  
Référence RNA : W872014916    
No de parution au journal officiel : 20190011

***
Liens    

- [Protections des données personnelles](https://ecrits-aout.fr/documents/notices/)
- [Colofon : crédits, ressources utilisées, licences, etc.](https://ecrits-aout.fr/documents/notices/)

***

<p class="text-muted">
Photo de Florencia Viadana (<a href="https://unsplash.com/photos/1iyGImW84cQ">Unsplash</a>)
</p>
